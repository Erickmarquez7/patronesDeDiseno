# Patrones de Diseño
Conocer los distintos patrones de diseño. 

Los ejemplos no necesariamente están relacionados entre sí. 

Son solo diferentes situaciones en que se podría aplicar dicho Patrón

(Debo añadir diagramas xD) 

## Principios del diseño:

1. Identificar lo que cambia de lo constante 

2. Programar la interfaz o super tipo, no la clase 

3. Favorecer la composición sobre la herencia

4. Esforzarse por hacer diseños debilemente acoplados,
esto da más flexibilidad

5. Las clases deben estar abiertas para extender, pero
cerradas para modificar

6. Dependa de abstracciones. No hacer
dependen de clases concretas. 
